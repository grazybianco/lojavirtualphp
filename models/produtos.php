<?php
class Produtos extends model
{

    public function getLista($partida=0,$limite=3)
    {
        $array = array();

        $sql = "SELECT *, (select nome_marca FROM tb_marcas WHERE id_marca = tb_produtos.id_marca)
        as nome_marca,
        (select nome_categoria FROM tb_categorias WHERE id_categorias = tb_produtos.id_categoria)
        as nome_categoria FROM tb_produtos LIMIT $partida,$limite";


        $sql = $this->db->query($sql);

        
        if ($sql->rowCount() > 0) {

            $array = $sql->fetchAll();

            foreach ($array as $key => $item) {

                $array[$key]['imagens'] = $this->getImagemByProdutoId($item['id_produto']);
            }
        }
        

        return $array;
    }
    public function getTotal(){
        $sql="SELECT COUNT(*) as c FROM tb_produtos";
        $sql=$this->db->query($sql);
        $sql=$sql->fetch();

        return $sql['c'];
    }
    public function getImagemByProdutoId($id_produto)
    {
        $array = array();

        $sql = "SELECT url FROM tb_produtos_imagens WHERE id_produto = :id_produto";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id_produto", $id_produto);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $array = $sql->fetchAll();
        }
        return $array;
    }

    
}
