<?php
class Marcas extends model
{

    public function getNameById($id)
    {

        $sql = "SELECT nome_marca FROM tb_marcas WHERE id_marca = :id";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":id", $id);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $data = $sql->fetch();
            return $data['nome_marca'];
        } else {
            return '';
        }
    }
}
