<?php

class Categorias extends model
{

	public function getLista()
	{
		$array = array();

		$sql = "SELECT * FROM tb_categorias ORDER BY categoria_pai DESC";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			foreach ($sql->fetchAll() as $item) {
				$item['subs'] = array();
				$array[$item['id_categorias']] = $item;
			}

			while ($this->stillNeed($array)) {
				$this->organizarCategoria($array);
			}
		}

		return $array;
	}

	public function getCategoriaArvore($id)
	{
		$array = array();

		$temFilho = true;

		while ($temFilho) {
			$sql = "SELECT * FROM tb_categorias WHERE id_categorias=:id";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(":id", $id);
			$sql->execute();
			if ($sql->rowCount() > 0) {
				$sql = $sql->fetch();
				$array[] = $sql;

				if (!empty($sql['categoria_pai'])) {
					$id = $sql['categoria_pai'];
				} else {
					$temFilho = false;
				}
			}
		}
		$array = array_reverse($array);

		return $array;
	}

	private function organizarCategoria(&$array)
	{
		foreach ($array as $id => $item) {
			if (isset($array[$item['categoria_pai']])) {
				$array[$item['categoria_pai']]['subs'][$item['id_categorias']] = $item;
				unset($array[$id]);
				break;
			}
		}
	}

	private function stillNeed($array)
	{
		foreach ($array as $item) {
			if (!empty($item['categoria_pai'])) {
				return true;
			}
		}

		return false;
	}
}
