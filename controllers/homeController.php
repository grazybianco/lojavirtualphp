<?php
class homeController extends controller
{

    private $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $dados = array();

        $produtos = new Produtos();
        $categorias = new Categorias();
        
        /*Paginação*/
        $paginaAtual = 1;
        $partida = 0;
        $limite = 3;

        if (!empty($_GET['p'])) {
            $paginaAtual = $_GET['p'];
        }
        $partida = ($paginaAtual * $limite) - $limite;

        $dados['lista'] = $produtos->getLista($partida, $limite);
        $dados['totalItens'] = $produtos->getTotal();
        $dados['numeroPaginas'] = ceil($dados['totalItens'] / $limite);
        $dados['paginaAtual'] = $paginaAtual;

        $dados['categorias'] = $categorias->getLista();

        $this->loadTemplate('home', $dados);
    }
}
