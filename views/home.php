<div class="row">
    <?php
    $a = 0;
    ?>
    <?php foreach ($lista as $produto_item) : ?>
        <div class="col-sm-4">
            <?php $this->loadView('produtos_itens', $produto_item); ?>
        </div>
        <?php
        if ($a >= 2) {
            $a = 0;
            echo '</div><div class="row">';
        } else {
            $a++;
        }
        ?>
    <?php endforeach; ?>
</div>
<div class="paginacaoArea">
    <?php for ($q = 1; $q <= $numeroPaginas; $q++) : ?>
        <div class="paginacaoItem <?php echo ($paginaAtual == $q) ? 'pagina_active' : ''; ?>">
            <a href="<?php echo BASE_URL; ?>?p=<?php echo $q ?>"><?php echo $q ?></a>
        </div>
    <?php endfor; ?>
</div>