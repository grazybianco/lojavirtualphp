<div class="produto_itens">
    <a href="">
        <div class="produto_tags">
            <?php if ($promocao == '1') : ?>
                <div class="produto_tag produto_tags_vermelha"><?php echo $this->lang->get('OFERTA'); ?></div>
            <?php endif; ?>
            <?php if ($mais_vendidos == '1') : ?>
                <div class="produto_tag produto_tags_verde"><?php echo $this->lang->get('MAIS_VENDIDOS'); ?></div>
            <?php endif; ?>
            <?php if ($novidade == '1') : ?>
                <div class="produto_tag produto_tags_azul"><?php echo $this->lang->get('LANCAMENTO'); ?></div>
            <?php endif; ?>
        </div>
        <div class="produto_imagem">
            <img src="<?php echo BASE_URL; ?>media/produtos/<?php echo $imagens[0]['url']; ?>" width="100%" />
        </div>
        <div class="produto_nome"><?php echo $nome_produto; ?></div>
        <div class="produto_marca"><?php echo $nome_marca; ?></div>
        <div class="produto_preco_de">
            <?php
            if ($preco_de != '0') {
                echo 'R$ ' . number_format($preco_de, 2, ',', '.');
            }

            ?>
        </div>
        <div class="produto_preco"><?php echo 'R$ ' . number_format($preco_produto, 2, ',', '.'); ?></div>
        <div style="clear:both"></div>
    </a>
</div>