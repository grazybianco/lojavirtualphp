<?php foreach ($subs as $sub) : ?>

    <li>
        <a href="<?php echo BASE_URL . 'categorias/enter/' . $sub['id_categorias']; ?>">
            <?php
            for ($q = 0; $q < $nivel; $q++) echo '&nbsp; &nbsp; ';
            echo $sub['nome_categoria']
            ?>
        </a>
    </li>
    <?php
    if (count($sub['subs']) > 0) {
        $this->loadView('menu_subcategorias', array(
            'subs' => $sub['subs'],
            'nivel' => $nivel+1
        ));
    }
    ?>
<?php endforeach; ?>